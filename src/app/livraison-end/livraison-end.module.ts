import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LivraisonEndPageRoutingModule } from './livraison-end-routing.module';

import { LivraisonEndPage } from './livraison-end.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LivraisonEndPageRoutingModule
  ],
  declarations: [LivraisonEndPage]
})
export class LivraisonEndPageModule {}
