import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LivraisonEndPage } from './livraison-end.page';

const routes: Routes = [
  {
    path: '',
    component: LivraisonEndPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LivraisonEndPageRoutingModule {}
