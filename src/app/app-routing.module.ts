import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'signup',
    loadChildren: () => import('./signup/signup.module').then( m => m.SignupPageModule)
  },
  {
    path: 'cgu',
    loadChildren: () => import('./cgu/cgu.module').then( m => m.CguPageModule)
  },
  {
    path: 'profile',
    loadChildren: () => import('./profile/profile.module').then( m => m.ProfilePageModule)
  },
  {
    path: 'user-location',
    loadChildren: () => import('./user-location/user-location.module').then( m => m.UserLocationPageModule)
  },
  {
    path: 'account-type',
    loadChildren: () => import('./account-type/account-type.module').then( m => m.AccountTypePageModule)
  },
  {
    path: 'fournisseur-infos',
    loadChildren: () => import('./fournisseur-infos/fournisseur-infos.module').then( m => m.FournisseurInfosPageModule)
  },
  {
    path: 'abonnement-fournisseur',
    loadChildren: () => import('./abonnement-fournisseur/abonnement-fournisseur.module').then( m => m.AbonnementFournisseurPageModule)
  },
  {
    path: 'mode-de-paiement-fournisseur',
    loadChildren: () => import('./mode-de-paiement-fournisseur/mode-de-paiement-fournisseur.module').then( m => m.ModeDePaiementFournisseurPageModule)
  },
  {
    path: 'home-fournisseur',
    loadChildren: () => import('./home-fournisseur/home-fournisseur.module').then( m => m.HomeFournisseurPageModule)
  },
  {
    path: 'stand',
    loadChildren: () => import('./stand/stand.module').then( m => m.StandPageModule)
  },
  {
    path: 'stand-infos',
    loadChildren: () => import('./stand-infos/stand-infos.module').then( m => m.StandInfosPageModule)
  },
  {
    path: 'livraison',
    loadChildren: () => import('./livraison/livraison.module').then( m => m.LivraisonPageModule)
  },
  {
    path: 'livraison-map',
    loadChildren: () => import('./livraison-map/livraison-map.module').then( m => m.LivraisonMapPageModule)
  },
  {
    path: 'livraison-end',
    loadChildren: () => import('./livraison-end/livraison-end.module').then( m => m.LivraisonEndPageModule)
  },
  {
    path: 'commandes',
    loadChildren: () => import('./commandes/commandes.module').then( m => m.CommandesPageModule)
  },
  {
    path: 'commande-infos',
    loadChildren: () => import('./commande-infos/commande-infos.module').then( m => m.CommandeInfosPageModule)
  },
  {
    path: 'finnance',
    loadChildren: () => import('./finnance/finnance.module').then( m => m.FinnancePageModule)
  },
  {
    path: 'factures',
    loadChildren: () => import('./factures/factures.module').then( m => m.FacturesPageModule)
  },
  {
    path: 'facture-infos',
    loadChildren: () => import('./facture-infos/facture-infos.module').then( m => m.FactureInfosPageModule)
  },
  {
    path: 'choisir-un',
    loadChildren: () => import('./choisir-un/choisir-un.module').then( m => m.ChoisirUnPageModule)
  },
  {
    path: 'choisir-un-plat',
    loadChildren: () => import('./choisir-un-plat/choisir-un-plat.module').then( m => m.ChoisirUnPlatPageModule)
  },
  {
    path: 'livraison-recuperer',
    loadChildren: () => import('./livraison-recuperer/livraison-recuperer.module').then( m => m.LivraisonRecupererPageModule)
  },
  {
    path: 'mode-livraison',
    loadChildren: () => import('./mode-livraison/mode-livraison.module').then( m => m.ModeLivraisonPageModule)
  },
  {
    path: 'passer-commande',
    loadChildren: () => import('./passer-commande/passer-commande.module').then( m => m.PasserCommandePageModule)
  },
  {
    path: 'restaurateur',
    loadChildren: () => import('./restaurateur/restaurateur.module').then( m => m.RestaurateurPageModule)
  },
  {
    path: 'restaurateur-infos',
    loadChildren: () => import('./restaurateur-infos/restaurateur-infos.module').then( m => m.RestaurateurInfosPageModule)
  },
  {
    path: 'restaurateur-carte',
    loadChildren: () => import('./restaurateur-carte/restaurateur-carte.module').then( m => m.RestaurateurCartePageModule)
  },
  {
    path: 'restaurateur-carte-infos',
    loadChildren: () => import('./restaurateur-carte-infos/restaurateur-carte-infos.module').then( m => m.RestaurateurCarteInfosPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
