import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { StandInfosPage } from './stand-infos.page';

const routes: Routes = [
  {
    path: '',
    component: StandInfosPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class StandInfosPageRoutingModule {}
