import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { StandInfosPageRoutingModule } from './stand-infos-routing.module';

import { StandInfosPage } from './stand-infos.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    StandInfosPageRoutingModule
  ],
  declarations: [StandInfosPage]
})
export class StandInfosPageModule {}
