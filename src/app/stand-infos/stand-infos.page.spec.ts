import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { StandInfosPage } from './stand-infos.page';

describe('StandInfosPage', () => {
  let component: StandInfosPage;
  let fixture: ComponentFixture<StandInfosPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StandInfosPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(StandInfosPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
