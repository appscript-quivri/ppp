import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ModeDePaiementFournisseurPage } from './mode-de-paiement-fournisseur.page';

describe('ModeDePaiementFournisseurPage', () => {
  let component: ModeDePaiementFournisseurPage;
  let fixture: ComponentFixture<ModeDePaiementFournisseurPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModeDePaiementFournisseurPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ModeDePaiementFournisseurPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
