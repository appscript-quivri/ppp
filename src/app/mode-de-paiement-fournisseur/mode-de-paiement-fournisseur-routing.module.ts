import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ModeDePaiementFournisseurPage } from './mode-de-paiement-fournisseur.page';

const routes: Routes = [
  {
    path: '',
    component: ModeDePaiementFournisseurPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ModeDePaiementFournisseurPageRoutingModule {}
