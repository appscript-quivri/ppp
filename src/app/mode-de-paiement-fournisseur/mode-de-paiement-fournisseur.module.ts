import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ModeDePaiementFournisseurPageRoutingModule } from './mode-de-paiement-fournisseur-routing.module';

import { ModeDePaiementFournisseurPage } from './mode-de-paiement-fournisseur.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ModeDePaiementFournisseurPageRoutingModule
  ],
  declarations: [ModeDePaiementFournisseurPage]
})
export class ModeDePaiementFournisseurPageModule {}
