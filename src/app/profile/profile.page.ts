import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFireDatabase } from '@angular/fire/database';
import { AngularFireStorage } from '@angular/fire/storage';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})

export class ProfilePage implements OnInit {
        image: string;
        filePath: string;
        task: any;
        userId: string;
        userData = {
            name: '',
            tel: '',
            mail: '',
            adress1: '',
            adress2: '',
            city: '',
            state: '',
            zipcode: '',
            imgUrl: '',
          };
        date = new Date().getTime();

  constructor(
      public afAuth: AngularFireAuth,
      public afDB: AngularFireDatabase,
      public afSG: AngularFireStorage,
      private router: Router,
      private camera: Camera
      ) {
          //verifier l'etat de connexion de l'utilisateur 
          this.afAuth.authState.subscribe(auth => {
          if (!auth) {
            this.router.navigateByUrl('/login');
            console.log('non connecté');
          } else {
            this.userId = auth.uid;
            this.getUserInfo();
            console.log('Connecté: ' + auth.uid);
          }
        });
      }

  ngOnInit() {
  }
    
    //recuperer les informations de l'utilisateur dans firebase 
    getUserInfo() {
      this.afDB.object('Users/' + this.userId).snapshotChanges()
      .subscribe(actions => {
        this.afSG.ref(actions.payload.exportVal().imgUrl).getDownloadURL().subscribe(url => {
          this.userData = {
            name: actions.payload.exportVal().name,
            tel: actions.payload.exportVal().tel,
            mail: actions.payload.exportVal().mail,
            adress1: actions.payload.exportVal().adress1,
            adress2: actions.payload.exportVal().adress2,
            city: actions.payload.exportVal().city,
            state: actions.payload.exportVal().state,
            zipcode: actions.payload.exportVal().zipcode, 
            imgUrl: url,
          };
        });
      });
    }
    
    //Ajouter une photo de profile 
    async addPhoto() {
      const base64 = await this.captureImage();
      this.createUploadTask(base64);
    }
    
    async captureImage() {
      const options: CameraOptions = {
        quality: 100,
        destinationType: this.camera.DestinationType.DATA_URL,
        encodingType: this.camera.EncodingType.JPEG,
        mediaType: this.camera.MediaType.PICTURE,
        targetWidth: 1000,
        targetHeight: 1000,
        sourceType: this.camera.PictureSourceType.PHOTOLIBRARY
      };
      return await this.camera.getPicture(options);
    }
    
    createUploadTask(file: string): void {
      this.filePath = 'post_' + this.date + '.jpg';
      this.image = 'data:image/jpg;base64,' + file;
    }
    
    //Enregistrer les modifications dans firebase
    publish() {
      this.task = this.afSG.ref(this.filePath).putString(this.image, 'data_url');
      this.task.then(res => {
        this.afDB.object('Users/' + this.userId).set({ 
            name: this.userData.name,
            tel: this.userData.tel,
            mail: this.userData.mail,
            adress1: this.userData.adress1,
            adress2: this.userData.adress2,
            city: this.userData.city,
            state: this.userData.state,
            zipcode: this.userData.zipcode, 
            imgUrl: this.filePath
        });
        this.router.navigateByUrl('/');
      });
    }

}
