import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RestaurateurInfosPage } from './restaurateur-infos.page';

describe('RestaurateurInfosPage', () => {
  let component: RestaurateurInfosPage;
  let fixture: ComponentFixture<RestaurateurInfosPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RestaurateurInfosPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RestaurateurInfosPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
