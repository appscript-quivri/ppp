import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RestaurateurInfosPage } from './restaurateur-infos.page';

const routes: Routes = [
  {
    path: '',
    component: RestaurateurInfosPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RestaurateurInfosPageRoutingModule {}
