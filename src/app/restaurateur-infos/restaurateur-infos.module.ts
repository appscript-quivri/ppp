import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RestaurateurInfosPageRoutingModule } from './restaurateur-infos-routing.module';

import { RestaurateurInfosPage } from './restaurateur-infos.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RestaurateurInfosPageRoutingModule
  ],
  declarations: [RestaurateurInfosPage]
})
export class RestaurateurInfosPageModule {}
