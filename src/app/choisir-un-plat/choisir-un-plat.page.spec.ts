import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ChoisirUnPlatPage } from './choisir-un-plat.page';

describe('ChoisirUnPlatPage', () => {
  let component: ChoisirUnPlatPage;
  let fixture: ComponentFixture<ChoisirUnPlatPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChoisirUnPlatPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ChoisirUnPlatPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
