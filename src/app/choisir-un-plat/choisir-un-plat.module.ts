import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ChoisirUnPlatPageRoutingModule } from './choisir-un-plat-routing.module';

import { ChoisirUnPlatPage } from './choisir-un-plat.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ChoisirUnPlatPageRoutingModule
  ],
  declarations: [ChoisirUnPlatPage]
})
export class ChoisirUnPlatPageModule {}
