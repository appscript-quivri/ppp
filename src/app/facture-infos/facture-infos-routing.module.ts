import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FactureInfosPage } from './facture-infos.page';

const routes: Routes = [
  {
    path: '',
    component: FactureInfosPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FactureInfosPageRoutingModule {}
