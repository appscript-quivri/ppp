import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FactureInfosPageRoutingModule } from './facture-infos-routing.module';

import { FactureInfosPage } from './facture-infos.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FactureInfosPageRoutingModule
  ],
  declarations: [FactureInfosPage]
})
export class FactureInfosPageModule {}
