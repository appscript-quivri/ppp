import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FactureInfosPage } from './facture-infos.page';

describe('FactureInfosPage', () => {
  let component: FactureInfosPage;
  let fixture: ComponentFixture<FactureInfosPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FactureInfosPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FactureInfosPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
