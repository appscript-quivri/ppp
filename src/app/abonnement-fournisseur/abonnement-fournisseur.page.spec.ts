import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AbonnementFournisseurPage } from './abonnement-fournisseur.page';

describe('AbonnementFournisseurPage', () => {
  let component: AbonnementFournisseurPage;
  let fixture: ComponentFixture<AbonnementFournisseurPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AbonnementFournisseurPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AbonnementFournisseurPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
