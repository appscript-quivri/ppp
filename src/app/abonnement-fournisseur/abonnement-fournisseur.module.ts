import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AbonnementFournisseurPageRoutingModule } from './abonnement-fournisseur-routing.module';

import { AbonnementFournisseurPage } from './abonnement-fournisseur.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AbonnementFournisseurPageRoutingModule
  ],
  declarations: [AbonnementFournisseurPage]
})
export class AbonnementFournisseurPageModule {}
