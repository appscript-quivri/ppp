import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AbonnementFournisseurPage } from './abonnement-fournisseur.page';

const routes: Routes = [
  {
    path: '',
    component: AbonnementFournisseurPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AbonnementFournisseurPageRoutingModule {}
