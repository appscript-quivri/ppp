import { Component, OnInit } from '@angular/core';
declare var Stripe;
//import { HttpClient } from "@angular/common/http";
import { LoadingController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-abonnement-fournisseur',
  templateUrl: './abonnement-fournisseur.page.html',
  styleUrls: ['./abonnement-fournisseur.page.scss'],
})
export class AbonnementFournisseurPage implements OnInit {
    stripe = Stripe('YOUR_PUBLISHABLE_KEY');
  card: any;

  constructor(
      public loadingController: LoadingController,
       private router: Router
      ) { }

  ngOnInit() {
     this.setupStripe(); 
  }
  
  
  setupStripe() {
    let elements = this.stripe.elements();
    var style = {
      base: {
        color: '#32325d',
        lineHeight: '24px',
        fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
        fontSmoothing: 'antialiased',
        fontSize: '16px',
        '::placeholder': {
          color: '#aab7c4'
        }
      },
      invalid: {
        color: '#fa755a',
        iconColor: '#fa755a'
      }
    };

    this.card = elements.create('card', { style: style });
    console.log(this.card);
    this.card.mount('#card-element');

    this.card.addEventListener('change', event => {
      var displayError = document.getElementById('card-errors');
      if (event.error) {
        displayError.textContent = event.error.message;
      } else {
        displayError.textContent = '';
      }
    });

    var form = document.getElementById('payment-form');
    form.addEventListener('submit', event => {
      event.preventDefault();
      console.log(event)

      this.stripe.createSource(this.card).then(result => {
        if (result.error) {
          var errorElement = document.getElementById('card-errors');
          errorElement.textContent = result.error.message;
        } else {
          console.log(result);
          //this.makePayment(result.id);
        }
      });
    });
  }
  
  payStripe() { 
      this.presentLoading();
      
  }
  
  async presentLoading() {
    const loading = await this.loadingController.create({
      cssClass: 'my-custom-class',
      message: 'Paiement en cours...',
      duration: 3000
    });
    await loading.present();

    const { role, data } = await loading.onDidDismiss();
    this.router.navigateByUrl('/mode-de-paiement-fournisseur'); 
  } 

}
