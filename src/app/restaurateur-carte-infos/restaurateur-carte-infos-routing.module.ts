import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RestaurateurCarteInfosPage } from './restaurateur-carte-infos.page';

const routes: Routes = [
  {
    path: '',
    component: RestaurateurCarteInfosPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RestaurateurCarteInfosPageRoutingModule {}
