import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RestaurateurCarteInfosPageRoutingModule } from './restaurateur-carte-infos-routing.module';

import { RestaurateurCarteInfosPage } from './restaurateur-carte-infos.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RestaurateurCarteInfosPageRoutingModule
  ],
  declarations: [RestaurateurCarteInfosPage]
})
export class RestaurateurCarteInfosPageModule {}
