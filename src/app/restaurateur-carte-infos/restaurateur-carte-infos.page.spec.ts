import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RestaurateurCarteInfosPage } from './restaurateur-carte-infos.page';

describe('RestaurateurCarteInfosPage', () => {
  let component: RestaurateurCarteInfosPage;
  let fixture: ComponentFixture<RestaurateurCarteInfosPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RestaurateurCarteInfosPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RestaurateurCarteInfosPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
