import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeFournisseurPage } from './home-fournisseur.page';

const routes: Routes = [
  {
    path: '',
    component: HomeFournisseurPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HomeFournisseurPageRoutingModule {}
