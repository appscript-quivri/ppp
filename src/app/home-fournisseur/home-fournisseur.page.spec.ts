import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { HomeFournisseurPage } from './home-fournisseur.page';

describe('HomeFournisseurPage', () => {
  let component: HomeFournisseurPage;
  let fixture: ComponentFixture<HomeFournisseurPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeFournisseurPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(HomeFournisseurPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
