import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HomeFournisseurPageRoutingModule } from './home-fournisseur-routing.module';

import { HomeFournisseurPage } from './home-fournisseur.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HomeFournisseurPageRoutingModule
  ],
  declarations: [HomeFournisseurPage]
})
export class HomeFournisseurPageModule {}
