import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LivraisonRecupererPageRoutingModule } from './livraison-recuperer-routing.module';

import { LivraisonRecupererPage } from './livraison-recuperer.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LivraisonRecupererPageRoutingModule
  ],
  declarations: [LivraisonRecupererPage]
})
export class LivraisonRecupererPageModule {}
