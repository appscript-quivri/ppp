import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LivraisonRecupererPage } from './livraison-recuperer.page';

const routes: Routes = [
  {
    path: '',
    component: LivraisonRecupererPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LivraisonRecupererPageRoutingModule {}
