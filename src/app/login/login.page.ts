import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { ToastController } from '@ionic/angular';

//import { Platform } from '@ionic/angular';
import { AngularFireDatabase } from '@angular/fire/database';
import { Router } from '@angular/router'; 

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
    passwordType: string = 'password';
    iconType: string = 'eye-outline';
    passwordShown: boolean = false;
    
    loginData = {
        email: '',
        password: ''
      }; 

  constructor(
      private router: Router, 
      public afDB: AngularFireDatabase,
      public toastController: ToastController,
      public afAuth: AngularFireAuth
      ) { }

  ngOnInit() {
  }
  
  //Password 
  public togglePassword() { 
      if(this.passwordShown) {
          this.passwordShown = false; 
          this.passwordType = 'password';
          this.iconType = 'eye-outline';
      } else {
          this.passwordShown = true; 
          this.passwordType = 'text';
          this.iconType = 'eye-off-outline';  
      }
  } 
  
   login() {
      this.afAuth.signInWithEmailAndPassword(this.loginData.email, this.loginData.password)
      .then(auth => {
          this.router.navigateByUrl('/home');   
        console.log('utilisateur connecté ' + auth.user.uid);
      }).catch(err => {
        this.errorMail();
      });
    }
    
    async errorMail() {
      const toast = await this.toastController.create({
        message: 'Email ou mot de passe incorrect',
        duration: 2000,
        position: 'top'
      });
      toast.present();
    }

}
