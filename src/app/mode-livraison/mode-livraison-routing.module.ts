import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ModeLivraisonPage } from './mode-livraison.page';

const routes: Routes = [
  {
    path: '',
    component: ModeLivraisonPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ModeLivraisonPageRoutingModule {}
