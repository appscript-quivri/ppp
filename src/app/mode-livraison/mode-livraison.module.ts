import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ModeLivraisonPageRoutingModule } from './mode-livraison-routing.module';

import { ModeLivraisonPage } from './mode-livraison.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ModeLivraisonPageRoutingModule
  ],
  declarations: [ModeLivraisonPage]
})
export class ModeLivraisonPageModule {}
