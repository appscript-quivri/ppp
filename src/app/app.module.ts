import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireStorageModule } from '@angular/fire/storage';

import { Camera } from '@ionic-native/camera/ngx';

export const firebaseConfig = {
   apiKey: "AIzaSyAV_-pSjC3JA0VezUtTrfk9YEITLrO4HkE",
    authDomain: "pousspousspanier-8a4aa.firebaseapp.com",
    databaseURL: "https://pousspousspanier-8a4aa.firebaseio.com",
    projectId: "pousspousspanier-8a4aa",
    storageBucket: "pousspousspanier-8a4aa.appspot.com",
    messagingSenderId: "37183061199",
    appId: "1:37183061199:web:8b3e575d5acb1894178bba" 
};

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
        BrowserModule,
        IonicModule.forRoot(), 
        AppRoutingModule,
        AngularFireModule.initializeApp(firebaseConfig),
        AngularFireDatabaseModule,
        AngularFireAuthModule,
        AngularFireStorageModule 
        ],
  providers: [
    StatusBar,
    SplashScreen,
    Camera, 
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
