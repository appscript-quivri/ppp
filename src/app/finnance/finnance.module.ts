import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FinnancePageRoutingModule } from './finnance-routing.module';

import { FinnancePage } from './finnance.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FinnancePageRoutingModule
  ],
  declarations: [FinnancePage]
})
export class FinnancePageModule {}
