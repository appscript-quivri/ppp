import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FinnancePage } from './finnance.page';

const routes: Routes = [
  {
    path: '',
    component: FinnancePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FinnancePageRoutingModule {}
