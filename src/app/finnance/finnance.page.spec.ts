import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FinnancePage } from './finnance.page';

describe('FinnancePage', () => {
  let component: FinnancePage;
  let fixture: ComponentFixture<FinnancePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FinnancePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FinnancePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
