import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RestaurateurPage } from './restaurateur.page';

const routes: Routes = [
  {
    path: '',
    component: RestaurateurPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RestaurateurPageRoutingModule {}
