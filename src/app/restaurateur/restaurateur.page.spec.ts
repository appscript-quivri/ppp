import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RestaurateurPage } from './restaurateur.page';

describe('RestaurateurPage', () => {
  let component: RestaurateurPage;
  let fixture: ComponentFixture<RestaurateurPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RestaurateurPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RestaurateurPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
