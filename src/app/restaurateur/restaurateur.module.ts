import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RestaurateurPageRoutingModule } from './restaurateur-routing.module';

import { RestaurateurPage } from './restaurateur.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RestaurateurPageRoutingModule
  ],
  declarations: [RestaurateurPage]
})
export class RestaurateurPageModule {}
