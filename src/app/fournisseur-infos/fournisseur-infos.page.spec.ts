import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FournisseurInfosPage } from './fournisseur-infos.page';

describe('FournisseurInfosPage', () => {
  let component: FournisseurInfosPage;
  let fixture: ComponentFixture<FournisseurInfosPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FournisseurInfosPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FournisseurInfosPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
