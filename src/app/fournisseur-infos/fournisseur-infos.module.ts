import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FournisseurInfosPageRoutingModule } from './fournisseur-infos-routing.module';

import { FournisseurInfosPage } from './fournisseur-infos.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FournisseurInfosPageRoutingModule
  ],
  declarations: [FournisseurInfosPage]
})
export class FournisseurInfosPageModule {}
