import { Component, OnInit } from '@angular/core';
import { ToastController } from '@ionic/angular';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFireDatabase } from '@angular/fire/database';
import { LoadingController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.page.html',
  styleUrls: ['./signup.page.scss'],
})
export class SignupPage implements OnInit {
    passwordType: string = 'password';
    iconType: string = 'eye-outline';
    passwordShown: boolean = false;
    signupData = { 
        email: '',
        password: '',
      };

  constructor(
      private router: Router, 
        public toastController: ToastController,
        public afAuth: AngularFireAuth,
        public afDB: AngularFireDatabase,
        public loadingController: LoadingController,
      ) { }

  ngOnInit() {
  }
  
  //Password 
  public togglePassword() { 
      if(this.passwordShown) {
          this.passwordShown = false; 
          this.passwordType = 'password';
          this.iconType = 'eye-outline';
      } else {
          this.passwordShown = true; 
          this.passwordType = 'text';
          this.iconType = 'eye-off-outline';  
      }
  }
  
  async signup() {  
      this.afAuth.createUserWithEmailAndPassword(this.signupData.email, this.signupData.password)
      .then(auth => { 
          this.router.navigateByUrl('/cgu');  
        console.log('ID de l utilisateur: ' + auth.user.uid); 
      }).catch(err => {
        console.log('Erreur: ' + err);  
        this.errorSignup();
      }); 
    }
    
    async errorSignup() {
      const toast = await this.toastController.create({
        message: 'Incorrect email or password', 
        duration: 2000,
        position: 'top'
      });
      toast.present();
    }

}
