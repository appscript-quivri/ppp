import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ChoisirUnPage } from './choisir-un.page';

const routes: Routes = [
  {
    path: '',
    component: ChoisirUnPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ChoisirUnPageRoutingModule {}
