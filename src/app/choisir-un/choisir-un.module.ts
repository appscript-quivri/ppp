import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ChoisirUnPageRoutingModule } from './choisir-un-routing.module';

import { ChoisirUnPage } from './choisir-un.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ChoisirUnPageRoutingModule
  ],
  declarations: [ChoisirUnPage]
})
export class ChoisirUnPageModule {}
