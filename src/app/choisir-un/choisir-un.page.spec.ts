import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ChoisirUnPage } from './choisir-un.page';

describe('ChoisirUnPage', () => {
  let component: ChoisirUnPage;
  let fixture: ComponentFixture<ChoisirUnPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChoisirUnPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ChoisirUnPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
