import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LivraisonMapPageRoutingModule } from './livraison-map-routing.module';

import { LivraisonMapPage } from './livraison-map.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LivraisonMapPageRoutingModule
  ],
  declarations: [LivraisonMapPage]
})
export class LivraisonMapPageModule {}
