import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LivraisonMapPage } from './livraison-map.page';

const routes: Routes = [
  {
    path: '',
    component: LivraisonMapPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LivraisonMapPageRoutingModule {}
