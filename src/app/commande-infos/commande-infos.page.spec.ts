import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CommandeInfosPage } from './commande-infos.page';

describe('CommandeInfosPage', () => {
  let component: CommandeInfosPage;
  let fixture: ComponentFixture<CommandeInfosPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommandeInfosPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CommandeInfosPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
