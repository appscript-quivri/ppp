import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CommandeInfosPage } from './commande-infos.page';

const routes: Routes = [
  {
    path: '',
    component: CommandeInfosPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CommandeInfosPageRoutingModule {}
