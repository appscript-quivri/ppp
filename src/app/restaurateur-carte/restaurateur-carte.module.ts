import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RestaurateurCartePageRoutingModule } from './restaurateur-carte-routing.module';

import { RestaurateurCartePage } from './restaurateur-carte.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RestaurateurCartePageRoutingModule
  ],
  declarations: [RestaurateurCartePage]
})
export class RestaurateurCartePageModule {}
