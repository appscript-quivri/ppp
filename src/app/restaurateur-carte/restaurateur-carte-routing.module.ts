import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RestaurateurCartePage } from './restaurateur-carte.page';

const routes: Routes = [
  {
    path: '',
    component: RestaurateurCartePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RestaurateurCartePageRoutingModule {}
