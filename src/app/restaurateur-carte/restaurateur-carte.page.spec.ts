import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RestaurateurCartePage } from './restaurateur-carte.page';

describe('RestaurateurCartePage', () => {
  let component: RestaurateurCartePage;
  let fixture: ComponentFixture<RestaurateurCartePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RestaurateurCartePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RestaurateurCartePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
